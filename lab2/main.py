import sys
import numpy as np

from placement import Plate


def main():
    matrix = np.loadtxt(sys.argv[1], delimiter=',')

    for (w, h) in ((5, 6), (3, 10), (2, 15)):
        debug = False
        plate = Plate(w, h, matrix, debug=debug)
        plate.make_initial_placement()
        print('the result is :\n{}\n'.format(plate.result))
        print('Q={}'.format(plate.optimality_criterion()))
        plate.optimize_placement()
        print('the result is :\n{}\n'.format(plate.result))
        print('Q={}'.format(plate.optimality_criterion()))

def for_report():
    matrix = np.loadtxt(sys.argv[1], delimiter=',')
    w, h = 5, 6
    plate = Plate(w, h, matrix, debug=True)
    plate.make_initial_placement()
    print('the result is :\n{}\n'.format(plate.result))
    print('Q={}'.format(plate.optimality_criterion()))
    plate.optimize_placement()
    print('the result is :\n{}\n'.format(plate.result))
    print('Q={}'.format(plate.optimality_criterion()))


if __name__ == '__main__':
    for_report()

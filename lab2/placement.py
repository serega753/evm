import itertools
import math

import numpy as np


def manhattan_distance(point1, point2):
    return np.sum(np.abs(point1 - point2))


def euclidean_distance(point1, point2):
    return np.sqrt(np.sum((point2 - point1) ** 2))


class Plate:
    def __init__(self, total_width, total_height, matrix, distance_function=manhattan_distance, debug=False):
        if (total_height * total_width) != matrix.shape[0]:
            raise ValueError('I don\'t want such args')

        self.matrix = matrix
        self.elems_number = matrix.shape[0]
        self.total_width = total_width
        self.total_height = total_height
        self.distance_function = distance_function

        self._result = np.zeros(shape=self.elems_number, dtype=np.intc)
        self.ros = None
        self.debug = debug

    def make_initial_placement(self):
        self._result[0] = 0
        placed = {0}
        rest = {*[i for i in range(1, self.elems_number)]}
        available_places = {1, self.__get_one_index_by_two(0, 1), self.__get_one_index_by_two(1, 0)}
        busy_places = {0}

        self.ros = np.sum(self.matrix, axis=1)
        print('ro values are:\n{}\n'.format(self.ros))

        stop = False
        while not stop:
            k_value_to_elems_mapping = {}
            for elem in rest:
                k_value = self.__evaluate_k(elem, placed)
                if k_value in k_value_to_elems_mapping:
                    k_value_to_elems_mapping[k_value].append(elem)
                else:
                    k_value_to_elems_mapping[k_value] = [elem, ]
            if self.debug:
                print('k_values:\n{}\n '.format(k_value_to_elems_mapping))
            self.__place(k_value_to_elems_mapping[max(k_value_to_elems_mapping)][:1], available_places, busy_places)
            for elem in k_value_to_elems_mapping[max(k_value_to_elems_mapping)][:1]:
                rest.remove(elem)
                placed.add(elem)
            if len(rest) == 0:
                stop = True

    def optimize_placement(self):
        opt_value = self.optimality_criterion()
        stop = False
        while not stop:
            l_values = np.zeros(self._result.shape)
            for i in range(self._result.shape[0]):
                l_values[i] = self.__evaluate_l_value(i)

            max_index = np.argmax(l_values)
            mass_center_x, mass_center_y = self.__evaluate_mass_center(max_index)
            x_values = np.zeros(2, dtype=np.intc)
            y_values = np.zeros(2, dtype=np.intc)
            x_values[1], x_values[0] = int(math.ceil(mass_center_x)), math.floor(mass_center_x)
            y_values[1], y_values[0] = int(math.ceil(mass_center_y)), math.floor(mass_center_y)

            if self.debug:
                print('--------------------------------------------\n\n')
                print('L values are {}'.format(l_values))
                print('maximum L value is {}'.format(l_values[max_index]))
                print('mass center for element {} is: X = {}; Y = {}'.format(max_index, mass_center_x, mass_center_y))
                print('we look for combinations of x_values = {}; y_values = {}'
                      .format(x_values, y_values))

            values = [x_values, y_values]
            result = itertools.product([0, 1], repeat=2)
            index_to_swap = None
            best_upgrade = None
            for variant in result:
                point_x, point_y = values[0][variant[0]], values[1][variant[1]]
                # if self.debug:
                    # print('point_x is {}; point_y is {}'.format(point_x, point_y))
                index = self.__get_one_index_by_two(*self.__get_indices_by_coordinates(point_x, point_y))
                if self.debug:
                    print('We are looking for effect of changing element T{}={} and element T{}={}'.format(max_index, self._result[max_index], index, self._result[index]))
                if index > self.elems_number:
                    continue
                upgrade = self.__evaluate_upgrade(max_index, index, l_values)
                if self.debug:
                    print('The upgrade is  {}'.format(upgrade))
                if best_upgrade is None or upgrade < best_upgrade:
                    if upgrade < 0:
                        best_upgrade = upgrade
                        index_to_swap = index

            if best_upgrade is None:
                break
            old_result = np.copy(self._result)
            self.__swap_indices(max_index, index_to_swap)
            if self.debug:
                print('we swap T{}={} and T{}={}'.format(max_index, self._result[max_index], index_to_swap, self._result[index_to_swap]))

            prev_opt_value = opt_value
            opt_value = self.optimality_criterion()
            if self.debug:
                print('the temp result is\n {}\n'.format(self.result))
                print('Q = {}'.format(opt_value))
                print('--------------------------------------------\n\n')
            if prev_opt_value - opt_value < 0:
                stop = True
                self._result = old_result

    @staticmethod
    def __get_center_by_indices(j_x, i_y):
        return np.array([j_x, i_y])

    @staticmethod
    def __get_indices_by_coordinates(x, y):
        return x, y

    def __get_two_indices_by_one(self, index):
        return int(index % self.total_width), int(index / self.total_width)

    def __get_one_index_by_two(self, j_x, i_y):
        return int(i_y * self.total_width + j_x)

    def __evaluate_distance(self, first, second):
        first_center = self.__get_center_by_indices(*self.__get_two_indices_by_one(first))
        second_center = self.__get_center_by_indices(*self.__get_two_indices_by_one(second))
        return self.distance_function(first_center, second_center)

    @property
    def result(self):
        arr = np.copy(self._result)
        return arr.reshape((self.total_height, self.total_width))

    def __evaluate_k(self, elem, placed):
        k_value = 0
        for pl_elem in placed:
            k_value += 2 * self.matrix[elem][pl_elem]
        k_value -= self.ros[elem]
        return k_value

    def __place(self, elems, available_places, busy_places):
        if self.debug:
            print('matrix is:\n{}\n'.format(self._result.reshape(self.total_height, self.total_width)))
            print('we place {}'.format(elems))
        for elem in elems:
            place = self.__get_best_place(available_places, busy_places)
            self._result[place] = elem
            available_places.remove(place)
            busy_places.add(place)
            i_y, j_x = self.__get_two_indices_by_one(place)
            possible_new = [self.__get_one_index_by_two(i_y - 1, j_x), self.__get_one_index_by_two(i_y + 1, j_x),
                            self.__get_one_index_by_two(i_y, j_x - 1), self.__get_one_index_by_two(i_y, j_x + 1)]
            for possible in possible_new:
                if possible not in busy_places and 0 <= possible < self.elems_number:
                    available_places.add(possible)

    def __get_best_place(self, available_places, busy_places):
        best_place = None
        best_dist = None
        for possible in available_places:
            dist = 0
            for busy in busy_places:
                dist += self.__evaluate_distance(possible, busy)
            if best_dist is None or dist < best_dist:
                best_dist = dist
                best_place = possible
        return best_place

    def __evaluate_l_value(self, index_in_res):
        l_value = 0
        elem = self._result[index_in_res]
        for i in range(self._result.shape[0]):
            second = self._result[i]
            l_value += self.__evaluate_distance(index_in_res, i) * self.matrix[elem][second]
        return l_value / self.ros[elem]

    def __evaluate_mass_center(self, index_in_res):
        center_x = 0
        center_y = 0
        elem = self._result[index_in_res]
        for i in range(self._result.shape[0]):
            second = self._result[i]
            x, y = self.__get_center_by_indices(*self.__get_two_indices_by_one(second))
            center_x += self.matrix[elem][second] * x
            center_y += self.matrix[elem][second] * y

        center_x /= self.ros[elem]
        center_y /= self.ros[elem]
        return center_x, center_y

    def __swap_indices(self, first, second):
        self._result[first], self._result[second] = self._result[second], self._result[first]

    def __evaluate_upgrade(self, first_index, second_index, l_values):
        old = np.copy(self._result)
        self.__swap_indices(first_index, second_index)
        first_l = self.__evaluate_l_value(first_index)
        second_l = self.__evaluate_l_value(second_index)
        delta_l = first_l + second_l - l_values[first_index] - l_values[second_index]
        self._result = old
        return delta_l

    def optimality_criterion(self):
        q_value = 0
        for i in range(len(self._result)):
            for j in range(i+1, len(self._result)):
                first = self._result[i]
                second = self._result[j]
                q_value += self.__evaluate_distance(i, j) * self.matrix[first][second]
        return q_value


def main():
    import sys
    matrix = np.loadtxt(sys.argv[1], delimiter=',')
    plate = Plate(3, 3, matrix)
    plate.make_initial_placement()
    print(plate.result, plate.optimality_criterion())
    plate.optimize_placement()
    print(plate.result, plate.optimality_criterion())


if __name__ == '__main__':
    main()

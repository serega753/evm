import numpy as np
import copy


def form_start_division(matrix, fragmentation_arr, debug=False):
    matrix_to_work = np.copy(matrix)
    current_to_real = {i: i for i in range(matrix_to_work.shape[0])}
    real_to_current = copy.deepcopy(current_to_real)
    vertices_sets = []
    frag_num = 0
    for frag_part in fragmentation_arr:
        if debug:
            frag_num += 1
            print('we form the frag part {}'.format(frag_num))
            np.savetxt('matrix_seq_{}.csv'.format(frag_num), matrix_to_work, fmt='%d', delimiter=',')
        if frag_part == matrix_to_work.shape[0]:
            vertices_sets.append({current_to_real[i] for i in range(matrix_to_work.shape[0])})
            if debug:
                print('the left fits the last set size')
                print('mapping from current to real: {}\n'.format(current_to_real))
            break
        sums = np.sum(matrix_to_work, axis=1)
        min_indices = np.where(sums == np.amin(sums))
        if debug:
            print('sums for each line is:\n{}\n'.format(sums), 'min indices:\n{}\n'.format(min_indices))
        if min_indices[0].shape[0] != 1:
            index = np.argmax(np.max(matrix_to_work, axis=1))
        else:
            index = min_indices[0][0]

        indices = np.argwhere(matrix_to_work[index, :] > 0)
        indices = np.append(indices, index)
        indices = __array_to_set(indices)
        indices = __make_fit_size(matrix_to_work, indices, sums, frag_part, debug=debug)

        temp = []
        for ind in indices:
            temp.append(current_to_real[ind])
        current_indices = np.array([*indices])
        indices.clear()
        for t in temp:
            indices.add(t)
        vertices_sets.append(indices)
        indices = np.array([*indices])
        indices.sort()
        for ind in indices:
            current_to_real.pop(real_to_current[ind])
            real_to_current.pop(ind)
        for key, value in real_to_current.items():
            sub = np.searchsorted(indices, key)
            real_to_current[key] -= sub
            current_to_real.pop(value)
            current_to_real[value - sub] = key
        if debug:
            print('we chose to fragmentation #{} indices: {}\n'.format(frag_num, current_indices))
            print('mapping from current to real: {}\n'.format(current_to_real))
        matrix_to_work = np.delete(matrix_to_work, current_indices, axis=0)
        matrix_to_work = np.delete(matrix_to_work, current_indices, axis=1)

    return vertices_sets


def __get_new_indices(matrix, indices_to_watch, already_added, number_to_add, debug=False):
    if debug:
        print('we watch to indices to watch: {}'.format(indices_to_watch))
        print('we have to add {} elements'.format(number_to_add))
    new_indices = set()
    added = 0
    keks = np.array([*indices_to_watch], dtype=np.intc)
    sums = np.sum(matrix[keks, :], axis=1)
    sorted_indices = np.flip(np.argsort(sums))
    for index in sorted_indices:
        possible = np.argwhere(matrix[keks[index]] > 0).flatten()
        keks_2 = np.flip(np.argsort(matrix[keks[index], :]))
        if debug:
            print('keks2 is {}'.format(keks_2))
        if debug:
            print('for index {} possible are: {}'.format(keks[index], possible))
        for index_1 in keks_2:
            if keks_2[index_1] not in already_added and matrix[keks[index]][keks_2[index_1]] > 0:
                if added >= number_to_add:
                    if debug:
                        print('we return these: {}\n'.format(new_indices))
                    return new_indices
                added += 1
                if debug:
                    print('the value in [{}][{}] is {}'.format(keks[index], keks_2[index_1], matrix[keks[index]][keks_2[index_1]]))
                new_indices.add(keks_2[index_1])
    # for index in indices_to_watch:
    #     possible = np.argwhere(matrix[index] > 0).flatten()
    #     if debug:
    #         print('for index {} possible are: {}'.format(index, possible))
    #     if added >= number_to_add:
    #         if debug:
    #             print('we return these: {}\n'.format(new_indices))
    #             return new_indices
    #     for index_1 in possible:
    #         if index_1 not in already_added:
    #             added += 1
    #             new_indices.add(index_1)
    if indices_to_watch == new_indices or len(new_indices) == 0:
        new_indices.clear()
        candidates = []
        for line_index in range(matrix.shape[0]):
            if line_index in indices_to_watch:
                continue
            sign = True
            for elem in indices_to_watch:
                if matrix[line_index][elem] != 0:
                    sign = False
            if sign:
                candidates.append(line_index)

        candidates = np.array(candidates, dtype=np.intc)
        if debug:
            print('CANDIDATES ARE: {}'.format(candidates))
        temp_matrix = matrix[candidates, :]
        sums = np.sum(temp_matrix, axis=1)
        index = np.argmin(sums)
        indices_to_add = np.where(matrix[candidates[index]] != 0)[0]
        indices_to_add = np.append(indices_to_add, index)
        for index in indices_to_add:
            new_indices.add(index)
    if debug:
        print('we add new indices: {}'.format(new_indices))
    return new_indices


def __make_fit_size(matrix_to_work, indices, sums, frag_part, debug=False):
    if debug:
        print('STEP 1: indices are: {}'.format(indices))
    already_added = {*indices}
    while len(indices) != frag_part:
        if len(indices) > frag_part:
            if debug:
                print('To much, have to reduce\n')
            indices_arr = np.array([*indices])
            deltas = []
            for index in indices_arr:
                outer_sum = 0
                for index_i in indices:
                    outer_sum += matrix_to_work[index][index_i]
                deltas.append(sums[index] - outer_sum)
            if debug:
                print('we count deltas: {}\n'.format(deltas))
            max_index = np.argmax(deltas)
            indices.remove(indices_arr[max_index])
        else:
            if debug:
                print('To little, have to widen\n')
            new_indices = __get_new_indices(matrix_to_work, indices, already_added, frag_part - len(indices), debug)

            for i in new_indices:
                indices.add(i)
                already_added.add(i)
    return indices


def __array_to_set(array_to_cast):
    return set(array_to_cast.flatten())


def get_optimal_variant(matrix, vertices_sets, debug=False):
    arr_len = len(vertices_sets)
    for i in range(arr_len - 1):
        if debug:
            print('----------------------\n')
            print('iteration {}'.format(i))
            print('first is {}'.format(vertices_sets[i]))
            print('others are:{}'.format(vertices_sets[i + 1:]))
        others = vertices_sets[i + 1:]
        __perform_swaps(i, matrix, vertices_sets[i], others, debug)
        if debug:
            print('----------------------\n')
    return vertices_sets


def __perform_swaps(first_num, matrix, first, others, debug=False):
    first_index_mapping = {}
    other_index_mapping = [{} for _ in range(len(others))]

    stop = False
    iter_num = 0
    while not stop:
        iter_num += 1
        if debug:
            print('again\n---------------------------\n\n')
        index = 0
        max_indices = []
        arrs = []
        for elem in first:
            first_index_mapping[index] = elem
            index += 1

        for k in range(len(others)):
            second = others[k]
            second_index_mapping = {}
            index = 0
            for elem in second:
                second_index_mapping[index] = elem
                index += 1

            arr = np.zeros(shape=(len(first), len(second)))
            for i in range(arr.shape[0]):
                for j in range(arr.shape[1]):
                    i_mapped = first_index_mapping[i]
                    j_mapped = second_index_mapping[j]
                    arr[i][j] = __count_delta(matrix, i_mapped, j_mapped, first, second)
            max_index = np.argwhere(arr == np.max(arr))[0]
            other_index_mapping[k] = second_index_mapping
            max_indices.append(max_index)
            arrs.append(arr)

        max_index = 0
        if debug:
            print('arrs are made from deltas:\n{}\n'.format(arrs))
            for i in range(len(arrs)):
                np.savetxt('deltas_first_{}_second_{}_iter_{}.csv'.format(first_num, first_num + i + 1, iter_num), X=arrs[i],
                           delimiter=',', fmt='%d')
            print('mapping for first:\n {}\n'.format(first_index_mapping))
            print('mapping for others:\n {}\n'.format(other_index_mapping))
        for i in range(len(max_indices)):
            cur_index = max_indices[i]
            max_i = max_indices[max_index][0]
            max_j = max_indices[max_index][1]
            if arrs[max_index][max_i][max_j] < arrs[i][cur_index[0]][cur_index[1]]:
                max_index = i

        arr = arrs[max_index]
        if arr[max_indices[max_index][0]][max_indices[max_index][1]] <= 0:
            return
        first_to_swap = first_index_mapping[max_indices[max_index][0]]
        second_to_swap = other_index_mapping[max_index][max_indices[max_index][1]]
        if debug:
            print('we swap {} and {}'.format(first_to_swap, second_to_swap))
        first.remove(first_to_swap)
        others[max_index].remove(second_to_swap)

        others[max_index].add(first_to_swap)
        first.add(second_to_swap)

        first_index_mapping.clear()
        other_index_mapping[max_index].clear()


def __count_delta(matrix, first_set_index, second_set_index, first_set, second_set, debug=False):
    val = 0
    for elem in second_set:
        val += matrix[first_set_index][elem]
        val -= matrix[second_set_index][elem]
    for elem in first_set:
        val -= matrix[first_set_index][elem]
        val += matrix[second_set_index][elem]
    val -= 2 * matrix[first_set_index][second_set_index]
    return val


def optimality_criterion(matrix, vertices_sets, debug=False):
    val = 0
    arr_len = len(vertices_sets)
    for i in range(arr_len):
        for j in range(i + 1, arr_len):
            for elem_1 in vertices_sets[i]:
                for elem_2 in vertices_sets[j]:
                    val += matrix[elem_1][elem_2]
    return val


def main():
    import sys
    if len(sys.argv) != 2:
        raise ValueError('Not enough args')
    matrix = np.loadtxt(sys.argv[1], delimiter=',')
    vertices_sets = [{0, 1}, {2, 3, 4}, {5, 6, 7}]
    print(optimality_criterion(matrix, vertices_sets))
    new_vertices = get_optimal_variant(matrix, vertices_sets)
    print(new_vertices)
    print(optimality_criterion(matrix, new_vertices))


if __name__ == '__main__':
    main()

class Fragmentation:
    def __init__(self, number):
        self.number = number
        self.current_frag = {1: self.number}
        self.started = False

    def __iter__(self):
        return self

    def get_frac_arr(self):
        arr = []
        for key in self.current_frag:
            cur_arr = [key for i in range(self.current_frag[key])]
            for elem in cur_arr:
                arr.append(elem)
        arr.reverse()
        return arr

    def __next__(self):
        if self.number in self.current_frag:
            raise StopIteration

        if not self.started:
            self.started = True
            return self.get_frac_arr()
        index = None
        for i in range(1, self.number + 1):
            if i in self.current_frag and self.current_frag[i] > 0:
                index = i
                break

        if self.current_frag[index] > 1:
            index_num = self.current_frag[index]
            self.current_frag[index] = 0
            self.current_frag[index + 1] = 1 if (index + 1) not in self.current_frag \
                else self.current_frag[index + 1] + 1
            adder = index_num * index - (index + 1)
            self.current_frag[1] = adder if 1 not in self.current_frag else self.current_frag[1] + adder
        else:
            self.current_frag[index] = 0
            next_index = None
            for i in range(index + 1, self.number + 1):
                if i in self.current_frag and self.current_frag[i] > 0:
                    next_index = i
                    break
            index_num = self.current_frag[next_index]
            self.current_frag[next_index] = 0
            self.current_frag[next_index + 1] = 1 if (next_index + 1) not in self.current_frag \
                else self.current_frag[next_index + 1] + 1
            adder = index + index_num * next_index - (next_index + 1)
            self.current_frag[1] = adder if 1 not in self.current_frag else self.current_frag[1] + adder
        return self.get_frac_arr()


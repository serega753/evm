import sys
import numpy as np
from fragmentation import Fragmentation
from componer import form_start_division, get_optimal_variant, optimality_criterion


def main():
    if len(sys.argv) != 2:
        raise ValueError('Not enough args')

    matrix = np.loadtxt(sys.argv[1], delimiter=',')
    best = None
    before = None
    best_frag = None
    best_division = None
    for cur_frag in Fragmentation(matrix.shape[0]):
        if {*cur_frag} <= {3, 4, 5, 6, 7}:
            start = form_start_division(matrix, cur_frag)
            before = optimality_criterion(matrix, start)
            final = get_optimal_variant(matrix, start)
            cur = optimality_criterion(matrix, final)
            if best is None or cur < best:
                best_frag = cur_frag
                best_division = final
                best = cur
                # before = optimality_criterion(matrix, start)
    print('Q = {}; Qbefore = {}; best fragmentation = {}; best division = {}'.format(best, before, best_frag,
                                                                                     best_division))


def for_report():
    print('sequantial algo\n')
    print('--------------------------------------------------------------------------------------------------\n')
    matrix = np.loadtxt(sys.argv[1], delimiter=',')
    frag = [7, 7, 7, 6, 3]
    start = form_start_division(matrix, frag, debug=True)
    before = optimality_criterion(matrix, start)
    print('Q={}'.format(before))
    print('--------------------------------------------------------------------------------------------------\n\n\n')
    print('iterational algo\n')
    final = get_optimal_variant(matrix, start, debug=True)
    print('final is {}'.format(final))
    optimal = optimality_criterion(matrix, final)
    print('Q={}'.format(optimal))


if __name__ == '__main__':
    # main()
    for_report()
